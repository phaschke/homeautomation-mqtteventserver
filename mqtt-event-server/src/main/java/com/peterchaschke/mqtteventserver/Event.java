package com.peterchaschke.mqtteventserver;

import java.util.ArrayList;

public class Event {
	
	private String name;
	private String event;
	private ArrayList<EventParameter> parameters;
	
	Event() {}
	
	Event(String name, String event, ArrayList<EventParameter> parameters) {
		this.setName(name);
		this.setEvent(event);
		this.setParameters(parameters);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public ArrayList<EventParameter> getParameters() {
		return parameters;
	}

	public void setParameters(ArrayList<EventParameter> parameters) {
		this.parameters = parameters;
	}
	
}
