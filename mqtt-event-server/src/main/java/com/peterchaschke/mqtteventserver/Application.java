package com.peterchaschke.mqtteventserver;

public class Application 
{
    public static void main( String[] args ) {
    	
    	MQTTClient client = new MQTTClient("tcp://localhost:1883", "event/in");
    	client.subscribe();

    }
}
