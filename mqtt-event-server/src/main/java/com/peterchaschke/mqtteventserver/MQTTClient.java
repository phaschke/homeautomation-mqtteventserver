package com.peterchaschke.mqtteventserver;

import java.util.ArrayList;
import java.util.UUID;

import org.eclipse.paho.client.mqttv3.IMqttClient;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttClientPersistence;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONArray;
import org.json.JSONObject;

public class MQTTClient implements MqttCallbackExtended {

	MqttClient client;
	String publisherId = UUID.randomUUID().toString();
	IMqttClient publisher;
	private String broker;
	private String topic;

	int qos = 2;

	public MQTTClient(String broker, String topic) {
		this.broker = broker;
		this.topic = topic;
	}

	public void subscribe() {

		String clientId = UUID.randomUUID().toString();

		try {

			MqttClientPersistence persistence = new MemoryPersistence();

			client = new MqttClient(broker, clientId, persistence);

			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setAutomaticReconnect(true);
			connOpts.setCleanSession(false);
			connOpts.setKeepAliveInterval(30);

			//System.out.println("Connecting to broker: " + broker);
			client.connect(connOpts);
			//System.out.println("Connected");

			client.setCallback(this);

			//System.out.println("Subscribe to channel: " + topic);
			client.subscribe(topic, 1);
			//System.out.println("Subscribed");

		} catch (IllegalArgumentException e) {

			throw new RuntimeException("Given broker URI is invalid.");

		} catch (MqttException e) {

			e.printStackTrace();
		}
	}

	public void connectionLost(Throwable cause) {

		//System.out.println("connectionLost");
	}

	public void messageArrived(String topic, MqttMessage message) throws Exception {

		// System.out.println("Recieved Response: [" + topic + "] " + message.toString());

		try {

			JSONObject jsonObj = new JSONObject(message.toString());
			if (jsonObj.has("name") && jsonObj.has("event")) {

				String controlName = jsonObj.get("name").toString();
				String event = jsonObj.get("event").toString();

				ArrayList<EventParameter> parametersList = new ArrayList<EventParameter>();

				if (jsonObj.has("params")) {
					JSONArray paramArr = jsonObj.getJSONArray("params");

					paramArr.forEach(item -> {
						JSONObject obj = (JSONObject) item;

						if (obj.has(obj.keySet().iterator().next().toString())) {
							String state = obj.get(obj.keySet().iterator().next().toString()).toString();
							parametersList.add(new EventParameter(obj.keySet().iterator().next().toString(), state));
						}
					});
				}

				Event eventObj = new Event(controlName, event, parametersList);

				jsonObj = new JSONObject(eventObj);
				//System.out.println(jsonObj.toString());

				RestClient restClient = new RestClient();
				//System.out.println("Send Rest Request");
				restClient.postRequest("http://localhost:8080/controls/event", jsonObj.toString());

			} else {
				System.out.println("Missing required event parameters.");
			}

		} catch (Exception e) {

			System.out.println("Exception in message event: " + e);
		}
	}

	public void deliveryComplete(IMqttDeliveryToken token) {
		// Do something on delivery complete...
	}

	public void connectComplete(boolean reconnect, String serverURI) {
		
		// Re-subscribe to the topic upon reconnect
		subscribe();
	}
}
